import React from "react";
import PropTypes from "prop-types";
import {Table, Button, CustomInput} from "reactstrap";

const RowItem = ({todo, toggleSelect, toggleComplete}) => {
    return (
        <tr>
            <td>
                <CustomInput
                    id={todo.id}
                    type="checkbox"
                    checked={todo.isSelect}
                    onChange={() => toggleSelect(todo.id)}
                />
            </td>
            <td>{todo.time.toDateString()}</td>
            <td>{todo.text}</td>
            <td>
                <Button className="ml-auto" color={todo.isComplete ? "danger" : "success"}
                        onClick={() => toggleComplete(todo.id)}>
                    {todo.isComplete ? "Complete" : "Running"}
                </Button>
            </td>
        </tr>
    )
};

RowItem.propTypes = {
    todo: PropTypes.object.isRequired,
    toggleSelect: PropTypes.func.isRequired,
    toggleComplete: PropTypes.func.isRequired
};

const TableView = ({todos, toggleSelect, toggleComplete}) => {
    console.log(todos)
    return (
        <Table>
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Text</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {todos.map((todo,index) => (
                <RowItem key={index + 1} todo={todo} toggleSelect={toggleSelect} toggleComplete={toggleComplete}/>
            ))}
            </tbody>
        </Table>
    )
};

TableView.propTypes = {
    todos: PropTypes.array.isRequired,
    toggleSelect: PropTypes.func.isRequired,
    toggleComplete: PropTypes.func.isRequired
};

export default TableView