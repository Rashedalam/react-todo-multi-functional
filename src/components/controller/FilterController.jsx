import React from "react";
import PropTypes from "prop-types";
import {ButtonGroup, Button} from "reactstrap";


const FilterController = ({handleFilter,filter}) => {
    return (
        <ButtonGroup>
            <Button color={filter === "all" ? "success" : "warning"} onClick={e => handleFilter('all')}>All</Button>
            <Button color={filter === "running" ? "success" : "warning"} onClick={e => handleFilter('running')}>Running</Button>
            <Button color={filter === "completed" ? "success" : "warning"} onClick={e => handleFilter('completed')}>Completed</Button>
        </ButtonGroup>
    )
};

FilterController.propTypes = {
    handleFilter: PropTypes.func.isRequired,
    filter: PropTypes.string.isRequired,
};

export default FilterController