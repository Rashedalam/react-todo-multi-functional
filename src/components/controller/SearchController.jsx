import React from "react";
import PropTypes from "prop-types";
import {Input, Button} from "reactstrap";

const SearchController = ({term, handleSearch, toggleForm}) => {
    return (
        <div className="d-flex">
            <Input
                type="text"
                className="mr-3"
                placeholder="Enter Search Keyword"
                value={term}
                onChange={e => handleSearch(e.target.value)}
            />
            <Button color="success" onClick={toggleForm}>New</Button>
        </div>
    )
};

SearchController.propTypes = {
    term: PropTypes.string.isRequired,
    handleSearch: PropTypes.func.isRequired,
    toggleForm: PropTypes.func.isRequired
};

export default SearchController