import React from "react";
import PropTypes from "prop-types";
import SearchController from "./SearchController";
import {Row, Col} from "reactstrap";
import FilterController from "./FilterController";
import ViewController from "./ViewController";
import BulkController from "./BulkController";

const Controller = ({term, handleSearch, toggleForm, handleFilter, changeView, view, clearCompleted, clearSelect, rest,filter}) => {
    return (
        <div>
            <SearchController term={term} toggleForm={toggleForm} handleSearch={handleSearch}/>
            <Row className="my-4">
                <Col md={{size: 4}}>
                    <FilterController handleFilter={handleFilter} filter={filter}/>
                </Col>
                <Col md={{size: 4}}>
                    <ViewController view={view} changeView={changeView}/>
                </Col>
                <Col md={{size: 4}} className="d-flex">
                    <div className="ml-auto">
                        <BulkController clearCompleted={clearCompleted} clearSelect={clearSelect} reset={rest}/>
                    </div>
                </Col>
            </Row>
        </div>
    )
};

Controller.propTypes = {
    term: PropTypes.string.isRequired,
    toggleForm: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
    handleFilter: PropTypes.func.isRequired,
    changeView: PropTypes.func.isRequired,
    view: PropTypes.string.isRequired,
    clearCompleted: PropTypes.func.isRequired,
    clearSelect: PropTypes.func.isRequired,
    rest: PropTypes.func.isRequired,
    filter: PropTypes.string.isRequired,

};

export default Controller