import React from "react";
import PropTypes from "prop-types";
import {Label, CustomInput} from "reactstrap";


const ViewController = ({view,changeView}) => {
    return (
        <div className="d-flex">
            <Label for="list-view">
                <CustomInput
                    id="list-view"
                    type="radio"
                    value="list"
                    name="view"
                    onChange={changeView}
                    className="d-inline-block"
                    checked={view === "list"}
                />
                List View
            </Label>
            <Label for="table-view" className="mx-4">
                <CustomInput
                    id="table-view"
                    type="radio"
                    value="table"
                    name="view"
                    onChange={changeView}
                    className="d-inline-block"
                    checked={view === "table"}
                />
                Table View
            </Label>
        </div>
    )
};

ViewController.propTypes = {
    view: PropTypes.string.isRequired,
    changeView: PropTypes.func.isRequired
};

export default ViewController