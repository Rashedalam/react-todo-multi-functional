import React, {Component} from "react";
import List from "../listView/Index";
import TableView from "../tableView/Index";

import {Modal, ModalHeader, ModalBody} from "reactstrap";
import Controller from "../controller/Index";
import TodoForm from "../form/index"
import shortId from "shortid";

class Index extends Component {

    state = {
        todos: [
            {
                id: "1",
                text: "This is text",
                description: "this is description",
                time: new Date(),
                isComplete: false,
                isSelect: false
            },
            {
                id: "2",
                text: "This is text",
                description: "this is description",
                time: new Date(),
                isComplete: false,
                isSelect: false
            }
        ],
        isModalOpen: false,
        term: "",
        view: "list",
        filter: "all",

    };

    toggleSelect = (todoId) => {
        const todos = [...this.state.todos];
        const todo = todos.find(todo => todo.id === todoId);
        todo.isSelect = !todo.isSelect;
        this.setState({
            todos
        })
    };

    toggleComplete = (todoId) => {
        const todos = [...this.state.todos];
        const todo = todos.find(todo => todo.id === todoId);
        todo.isComplete = !todo.isComplete;
        this.setState({
            todos
        })
    };

    handleSearch = (value) => {
        this.setState({
            term: value
        })
    };

    performSearch = () => {
        return this.state.todos.filter(todo => todo.text.toLowerCase().includes(this.state.term.toLowerCase()))
    };

    toggleForm = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
    };

    createTodo = (todo) => {

        todo.id = shortId.generate();
        todo.isSelect = false;
        todo.isComplete = false;
        todo.time = new Date();

        const todos = [todo, ...this.state.todos]

        this.setState({todos});
        this.toggleForm();
    };

    handleFilter = (value) => {
        this.setState({
            filter: value
        })
    };

    performFilter = (todos) => {
        const {filter} = this.state;
        if (filter === "running") {
            return todos.filter(todo => !todo.isComplete)
        } else if (filter === "completed") {
            return todos.filter(todo => todo.isComplete)
        } else {
            return todos
        }
    };

    changeView = (event) => {
        this.setState({
            view: event.target.value
        })
    };

    clearCompleted = () => {

    };

    clearSelect = () => {

    };

    reset = () => {

    };

    getView = () => {
        let todos = this.performSearch();
        todos = this.performFilter(todos);
        return this.state.view === "list" ? (
            <List todos={todos} toggleSelect={this.toggleSelect}
                  toggleComplete={this.toggleComplete}/>
        ) : (
            <TableView todos={todos} toggleSelect={this.toggleSelect}
                       toggleComplete={this.toggleComplete}/>
        )
    };


    render() {
        return (
            <div>
                <h1 className="text-center my-5">Stack Todo</h1>
                <Controller filter={this.state.filter} handleFilter={this.handleFilter} changeView={this.changeView}
                            clearCompleted={this.clearCompleted} clearSelect={this.clearSelect} rest={this.reset}
                            view={this.state.view} term={this.state.term} toggleForm={this.toggleForm}
                            handleSearch={this.handleSearch}/>
                <div>
                    {
                        this.getView()
                    }
                </div>

                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleForm}>
                    <ModalHeader toggle={this.toggleForm}>
                        Create New Todo Item
                    </ModalHeader>
                    <ModalBody>
                        <TodoForm createTodo={this.createTodo}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}

export default Index